import { ValidatorFn, FormGroup, ValidationErrors } from "@angular/forms";

export const passwordMatchValidator: ValidatorFn = (formGroup: FormGroup): ValidationErrors | null => {
    return formGroup.get('newPassword').value === formGroup.get('confirmPassword').value ?
        null : { 'passwordMismatch': true };
}