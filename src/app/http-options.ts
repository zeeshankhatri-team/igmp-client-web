import { HttpHeaders } from '@angular/common/http';

export const HTTP_OPTIONS: IHttpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};

export interface IHttpOptions {
    headers?: HttpHeaders | {
        [header: string]: string | string[];
    };
}
