import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule,
  MatListModule, MatAutocompleteModule, MatFormFieldModule,
  MatInputModule, MatDatepickerModule, MatNativeDateModule, MatTableModule,
  MatPaginatorModule, MatSortModule, MatProgressSpinnerModule, MatExpansionModule, MatCardModule, MatTooltipModule, MAT_FORM_FIELD_DEFAULT_OPTIONS, MAT_LABEL_GLOBAL_OPTIONS
} from '@angular/material';
import { NavComponent } from './nav/nav.component';
import { SearchComponent } from './search/search.component';
import { SearchParamComponent } from './search/search-param/search-param.component';
import { AppRoutingModule } from './/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpHeaders, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HTTP_OPTIONS } from './http-options';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { AuthGuard } from './@guards/auth.guard';
import { AlertService } from './@services/alert.service';
import { AuthService } from './@services/auth.service';
import { JwtInterceptor } from './@interceptors/jwt.interceptor';
import { ErrorInterceptor } from './@interceptors/error.interceptor';
import { HomeComponent } from './home/home.component';
import { BaseComponent } from './base/base.component';
import { LogoutComponent } from './auth/logout/logout.component';
import { AlertComponent } from './@directives/alert.component';
import { VerifyComponent } from './auth/verify/verify.component';
import { ResetComponent } from './auth/reset/reset.component';
import { ForgotComponent } from './auth/forgot/forgot.component';
import { ChangeComponent } from './auth/change/change.component';
import { NotificationListComponent } from './notification/notification-list/notification-list.component';
import { SingleNotificationUpsertComponent } from './notification/single-notification-upsert/single-notification-upsert.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    SearchComponent,
    SearchParamComponent,
    NotificationListComponent,
    SingleNotificationUpsertComponent,
    AlertComponent,
    LoginComponent,
    LogoutComponent,
    RegisterComponent,
    VerifyComponent,
    ResetComponent,
    ForgotComponent,
    ChangeComponent,
    HomeComponent,
    BaseComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    LayoutModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatTooltipModule
  ],
  providers: [
    AuthGuard,
    AlertService,
    AuthService,
    { provide: 'HttpOptions', useValue: HTTP_OPTIONS},
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: {appearance: "outline"}},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
