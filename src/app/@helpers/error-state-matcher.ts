import { ErrorStateMatcher } from '@angular/material';
import { FormGroupDirective, FormControl, NgForm } from '@angular/forms';

/** Error when invalid control is dirty, touched, or submitted. */
export class FieldErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
      const isSubmitted = form && form.submitted;
      return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}

/** Error when the parent is invalid */
export class CrossFieldErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    // return control.dirty && form.invalid;
    const isSubmitted = form && form.submitted;
    const isFormInvalid = form && form.invalid;
      return !!(control && (control.invalid || isFormInvalid) && (control.dirty || control.touched || isSubmitted));
  }
}
