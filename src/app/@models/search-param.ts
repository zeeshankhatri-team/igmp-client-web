export interface SearchParam {
    importerName: string | null;
    consignorName: string | null;
    itemDescription: string | null;
    blNumber: string | null;
    flightNumber: string | null;
    shed: string | null;
    blDateFrom: Date | null;
    blDateTo: Date | null;
    arrivalDateFrom: Date | null;
    arrivalDateTo: Date | null;
    igmDateFrom: Date | null;
    igmDateTo: Date | null;
    igmNumber: number | null;
}


