export interface SearchItem {
    id: number;
    indexNumber: string;
    importerName: string;
    consignorName: string;
    itemDescription: string;
    blNumber: string;
    flightNumber: string;
    shed: string;
    blDate: Date;
    arrivalDate: Date;
    igmDate: Date;
    igmNumber: number;
}
