export class PagedSorted<T> {
    pageIndex = 0;
    pageSize = 25;
    sortBy: string;
    direction: string;
    parameters: T;
}
