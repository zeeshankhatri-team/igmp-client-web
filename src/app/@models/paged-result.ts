export class PagedResult<T> {
    length: number;
    items: T[];
}
