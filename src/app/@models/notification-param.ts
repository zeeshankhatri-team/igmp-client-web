import { NotificationType } from "./notification";

export interface NotificationParam {
    active: boolean | null;
    NotificationType: NotificationType | null;
}
