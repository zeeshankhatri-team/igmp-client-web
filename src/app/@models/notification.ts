export interface Notification {
    id: number;
    notificationType: NotificationType;
    importerName: string;
    consignorName: string;
    itemDescription: string;
    blNumber: string;
    lastExecutedOn: Date | null;
    executionResultCount: number | null;
}

export enum NotificationType
{
    Single = 1,
    Continuous = 2,
}

export interface ContinuousNotification
{
    importerName: string;
    consignorName: string;
    itemDescription: string;
}

export interface SingleNotification
{
    blNumber: string;
}