﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { AlertService } from '../../@services/alert.service';
import { FieldErrorStateMatcher } from '../../@helpers/error-state-matcher';
import { NotificationService } from 'src/app/@services/notification.service';
import { FormMode } from 'src/app/@enums/form-mode';

@Component({
    selector: 'app-single-notification-upsert',
    templateUrl: './single-notification-upsert.component.html',
    styleUrls: ['./single-notification-upsert.component.css']
})
export class SingleNotificationUpsertComponent implements OnInit {
    singleNotificationForm: FormGroup;
    loading: boolean = false;
    submitted: boolean = false;
    id: number = 0;
    mode: FormMode = FormMode.Add;

    matcher = new FieldErrorStateMatcher();
    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private notificationService: NotificationService,
        private alertService: AlertService) { }

    blNumberFormControl = new FormControl('', [
        Validators.required,
    ]);

    ngOnInit() {
        this.singleNotificationForm = this.formBuilder.group({
            blNumber: this.blNumberFormControl});
    }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.singleNotificationForm.invalid) {
            return;
        }

        this.loading = true;
        switch(this.mode)
        {
            case FormMode.Add:
                this.notificationService.addSingle(this.singleNotificationForm.value)
                .subscribe(
                    () => {
                        this.alertService.success('Notification has been added successfully.', true);
                        this.router.navigate(['/']);
                    },
                    error => {
                        this.alertService.error(error);
                        this.loading = false;
                    });
                break;
            case FormMode.Update:
                this.notificationService.updateSingle(this.id, this.singleNotificationForm.value)
                .subscribe(
                    () => {
                        this.alertService.success('Notification has been updated successfully.', true);
                        this.router.navigate(['/']);
                    },
                    error => {
                        this.alertService.error(error);
                        this.loading = false;
                    });
                break;
        }
    }
}
