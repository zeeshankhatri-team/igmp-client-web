import { DataSource, CollectionViewer } from '@angular/cdk/collections';
import { catchError, finalize, tap, map } from 'rxjs/operators';
import { Observable, BehaviorSubject, of, merge } from 'rxjs';
import { PagedSorted } from '../../@models/paged-sorted';
import { PagedResult } from '../../@models/paged-result';
import { MatPaginator, MatSort } from '@angular/material';
import { Notification } from '../../@models/notification';
import { NotificationService } from 'src/app/@services/notification.service';
import { NotificationParam } from 'src/app/@models/notification-param';

export class NotificationDataSource extends DataSource<Notification> {

  private notificationsSubject = new BehaviorSubject<Notification[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(private notificationService: NotificationService, private paginator: MatPaginator, private sort: MatSort) {
    super();
  }

  connect(collectionViewer: CollectionViewer): Observable<Notification[]> {

    // Set the paginators length
    this.paginator.length = 0;

    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.paginator.page, this.sort.sortChange).pipe(
      tap(() => this.loadNotificationsPage())
    )
    .subscribe();

    return this.notificationsSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer) {
    this.notificationsSubject.complete();
    this.loadingSubject.complete();
  }

  loadNotificationsPage() {
    const pagedSorted = new PagedSorted<NotificationParam>();
    pagedSorted.pageIndex = this.paginator.pageIndex;
    pagedSorted.pageSize = this.paginator.pageSize;
    pagedSorted.sortBy = this.sort.active;
    pagedSorted.direction = this.sort.direction;
    pagedSorted.parameters = null;

    this.loadNotifications(pagedSorted);
  }

  loadNotifications(pagedSorted: PagedSorted<NotificationParam>) {
    this.paginator.pageIndex = pagedSorted.pageIndex;
    //this.params = pagedSorted.parameters;
    this.loadingSubject.next(true);
    this.notificationService.getAllPaged().pipe(
        catchError(() => of([])),
        finalize(() => this.loadingSubject.next(false)),
        tap((result: PagedResult<Notification>) => this.paginator.length = result.length),
        map((result: PagedResult<Notification>) => result.items)
      )
      .subscribe(items => this.notificationsSubject.next(items));
  }
}
