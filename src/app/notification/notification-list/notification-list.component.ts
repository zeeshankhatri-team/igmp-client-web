import { Component, OnInit, ViewChild } from '@angular/core';
import { PagedSorted } from '../../@models/paged-sorted';
import { MatPaginator, MatSort } from '@angular/material';
import { NotificationDataSource } from './notification-datasource';
import { NotificationService } from 'src/app/@services/notification.service';
import { NotificationType } from 'src/app/@models/notification';
import { NotificationParam } from 'src/app/@models/notification-param';

@Component({
  selector: 'app-notification-list',
  templateUrl: './notification-list.component.html',
  styleUrls: ['./notification-list.component.css']
})
export class NotificationListComponent implements OnInit {

  constructor(private service: NotificationService) { }

  NotificationType = NotificationType;
  dataSource: NotificationDataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = [
    'notificationType',
    'importerName',
    'consignorName',
    'itemDescription',
    'blNumber',
    'lastExecutedOn',
    'executionResultCount',
    'actions'
  ];

  ngOnInit() {
    this.dataSource = new NotificationDataSource(this.service, this.paginator, this.sort);
    this.load();
  }

  load(): void {
    const parameters = new PagedSorted<NotificationParam>();
    parameters.parameters = null;
    this.dataSource.loadNotifications(parameters);
  }

  onDelete(id: number)
  {
    this.service.delete(id).subscribe(() => {
      this.load();
    });
  }

  onEdit(id: number)
  {

  }

  onAdd()
  {

  }
}
