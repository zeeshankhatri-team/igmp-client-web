import { Component, OnInit, ViewChild } from '@angular/core';
import { SearchService } from '../@services/search.service';
import { SearchItem } from '../@models/search-item';
import { SearchResultDataSource } from './search-result-datasource';
import { SearchParam } from '../@models/search-param';
import { PagedSorted } from '../@models/paged-sorted';
import { MatPaginator, MatSort } from '@angular/material';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(private service: SearchService) { }

  dataSource: SearchResultDataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = [
    'importerName',
    'consignorName',
    'itemDescription',
    'blNumber',
    'flightNumber',
    'shed',
    'blDate',
    'arrivalDate',
    'igmDate',
    'igmNumber',
    'indexNumber',
    'detail'
  ];

  ngOnInit() {
    this.dataSource = new SearchResultDataSource(this.service, this.paginator, this.sort);
  }

  onSearch(searchParam: SearchParam): void {
    const parameters = new PagedSorted<SearchParam>();
    parameters.parameters = searchParam;
    this.dataSource.loadSearchItems(parameters);
  }

}
