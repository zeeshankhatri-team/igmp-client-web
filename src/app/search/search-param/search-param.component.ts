import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, AbstractControl, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { startWith, map, debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { FilterService } from '../../@services/filter.service';

export interface AutoCompleteEntity {
  name: string;
}

@Component({
  selector: 'app-search-param',
  templateUrl: './search-param.component.html',
  styleUrls: ['./search-param.component.css']
})
export class SearchParamComponent implements OnInit {

  @Output() search: EventEmitter<any> = new EventEmitter();

  constructor(private service: FilterService) {}

  importerNameFormControl = new FormControl('');
  consignorNameFormControl = new FormControl('');
  itemDescriptionFormControl = new FormControl('');
  blNumberFormControl = new FormControl('');
  flightNumberFormControl = new FormControl('');
  shedFormControl = new FormControl('');
  blDateFromFormControl = new FormControl('');
  blDateToFormControl = new FormControl('');
  arrivalDateFromFormControl = new FormControl('');
  arrivalDateToFormControl = new FormControl('');
  igmDateFromFormControl = new FormControl('');
  igmDateToFormControl = new FormControl('');
  igmNumberFormControl = new FormControl('', Validators.pattern('[0-9]*'));
  indexNumberFormControl = new FormControl('', Validators.pattern('[0-9]*'));


  searchParamForm = new FormGroup({
    importerName: this.importerNameFormControl,
    consignorName: this.consignorNameFormControl,
    itemDescription: this.itemDescriptionFormControl,
    blNumber: this.blNumberFormControl,
    flightNumber: this.flightNumberFormControl,
    shed: this.shedFormControl,
    blDateFrom: this.blDateFromFormControl,
    blDateTo: this.blDateToFormControl,
    arrivalDateFrom: this.arrivalDateFromFormControl,
    arrivalDateTo: this.arrivalDateToFormControl,
    igmDateFrom: this.igmDateFromFormControl,
    igmDateTo: this.igmDateToFormControl,
    igmNumber: this.igmNumberFormControl,
    indexNumber: this.indexNumberFormControl,
  });

  importerNameFilteredOptions: Observable<string[]>;
  consignorNameFilteredOptions: Observable<string[]>;
  itemDescriptionFilteredOptions: Observable<string[]>;
  blNumberFilteredOptions: Observable<string[]>;
  flightNumberFilteredOptions: Observable<string[]>;
  shedFilteredOptions: Observable<string[]>;

  blDateFromMinDate = new Date(2000, 1, 1);
  blDateFromMaxDate = new Date();

  blDateToMinDate = new Date();
  blDateToMaxDate = new Date();

  arrivalDateFromMinDate = new Date(2000, 1, 1);
  arrivalDateFromMaxDate = new Date();

  arrivalDateToMinDate = new Date();
  arrivalDateToMaxDate = new Date();

  igmDateFromMinDate = new Date(2000, 1, 1);
  igmDateFromMaxDate = new Date();

  igmDateToMinDate = new Date();
  igmDateToMaxDate = new Date();

  ngOnInit() {
    const controls = this.searchParamForm.controls;
    this.importerNameFilteredOptions = this.bindFilteredOptions(controls['importerName'], this.service.getImporters);
    this.consignorNameFilteredOptions = this.bindFilteredOptions(controls['consignorName'], this.service.getConsignors);
    this.itemDescriptionFilteredOptions = this.bindFilteredOptions(controls['itemDescription'], this.service.getItems);
    this.blNumberFilteredOptions = this.bindFilteredOptions(controls['blNumber'], this.service.getBLNumbers);
    this.flightNumberFilteredOptions = this.bindFilteredOptions(controls['flightNumber'], this.service.getFlightNumbers);
    this.shedFilteredOptions = this.bindFilteredOptions(controls['shed'], this.service.getSheds);
  }

  clear(control: AbstractControl) {
    control.setValue('');
  }

  onSubmit() {
    this.search.emit(this.searchParamForm.value);
  }

  blDateFromChanged() {
    this.blDateToMinDate = new Date(this.searchParamForm.controls['blDateFrom'].value);
  }

  arrivalDateFromChanged() {
    this.arrivalDateToMinDate = new Date(this.searchParamForm.controls['arrivalDateFrom'].value);
  }

  igmDateFromChanged() {
    this.igmDateToMinDate = new Date(this.searchParamForm.controls['igmDateFrom'].value);
  }

  private bindFilteredOptions(control: AbstractControl, source: Function): Observable<string[]> {
    source = source.bind(this.service);
    return control.valueChanges
      .pipe(
        debounceTime(200),
        distinctUntilChanged(),
        switchMap<Observable<any>, Observable<string[]>>(value => source(value))
      );
  }

  // private bindFilteredOptionsTemp(control: AbstractControl): any {
  //   return control.valueChanges
  //     .pipe(
  //       startWith(null),
  //       debounceTime(200),
  //       distinctUntilChanged(),
  //       switchMap(val => {
  //         return this.filter(val || '');
  //       })
  //     );
  // }

  // private filter(value: string): Observable<string[]> {
  //   const filterValue = value.toLowerCase();

  //   return this.service.getData()
  //     .pipe(
  //       map(response => response.filter(option => {
  //         return option.toLowerCase().indexOf(filterValue) === 0;
  //       })));
  // }
}
