import { DataSource, CollectionViewer } from '@angular/cdk/collections';
import { catchError, finalize, tap, map } from 'rxjs/operators';
import { Observable, BehaviorSubject, of, merge } from 'rxjs';
import { SearchItem } from '../@models/search-item';
import { SearchService } from '../@services/search.service';
import { SearchParam } from '../@models/search-param';
import { PagedSorted } from '../@models/paged-sorted';
import { PagedResult } from '../@models/paged-result';
import { MatPaginator, MatSort } from '@angular/material';

export class SearchResultDataSource extends DataSource<SearchItem> {

  private searchItemsSubject = new BehaviorSubject<SearchItem[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);
  private searchParam: SearchParam;

  public loading$ = this.loadingSubject.asObservable();

  constructor(private searchService: SearchService, private paginator: MatPaginator, private sort: MatSort) {
    super();
  }

  connect(collectionViewer: CollectionViewer): Observable<SearchItem[]> {

    // Set the paginators length
    this.paginator.length = 0;

    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.paginator.page, this.sort.sortChange).pipe(
      tap(() => this.loadSearchItemsPage())
    )
    .subscribe();

    return this.searchItemsSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer) {
    this.searchItemsSubject.complete();
    this.loadingSubject.complete();
  }

  loadSearchItemsPage() {
    const pagedSorted = new PagedSorted<SearchParam>();
    pagedSorted.pageIndex = this.paginator.pageIndex;
    pagedSorted.pageSize = this.paginator.pageSize;
    pagedSorted.sortBy = this.sort.active;
    pagedSorted.direction = this.sort.direction;
    pagedSorted.parameters = this.searchParam;

    this.loadSearchItems(pagedSorted);
  }

  loadSearchItems(pagedSorted: PagedSorted<SearchParam>) {
    this.paginator.pageIndex = pagedSorted.pageIndex;
    this.searchParam = pagedSorted.parameters;
    this.loadingSubject.next(true);
    this.searchService.search(pagedSorted).pipe(
        catchError(() => of([])),
        finalize(() => this.loadingSubject.next(false)),
        tap((result: PagedResult<SearchItem>) => this.paginator.length = result.length),
        map((result: PagedResult<SearchItem>) => result.items)
      )
      .subscribe(items => this.searchItemsSubject.next(items));
  }
}
