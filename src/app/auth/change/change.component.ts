﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { AlertService } from '../../@services/alert.service';
import { FieldErrorStateMatcher, CrossFieldErrorStateMatcher } from '../../@helpers/error-state-matcher';
import { AuthService } from '../../@services/auth.service';
import { passwordMatchValidator } from '../../@validators/password-match.validator';

@Component({
    selector: 'app-change',
    templateUrl: './change.component.html',
    styleUrls: ['./change.component.css']
})
export class ChangeComponent implements OnInit {
    changeForm: FormGroup;
    loading = false;
    submitted = false;

    matcher = new FieldErrorStateMatcher();
    crossMatcher = new CrossFieldErrorStateMatcher();

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private authService: AuthService,
        private alertService: AlertService) { }


    currentPasswordFormControl = new FormControl('', [
        Validators.required,
    ]);

    newPasswordFormControl = new FormControl('', [
        Validators.required,
    ]);

    confirmPasswordFormControl = new FormControl('', [
        Validators.required,
    ]);

    ngOnInit() {
        this.changeForm = this.formBuilder.group({
            currentPassword: this.currentPasswordFormControl,
            newPassword: this.newPasswordFormControl,
            confirmPassword: this.confirmPasswordFormControl,
        }, { validator: passwordMatchValidator });
    }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.changeForm.invalid) {
            return;
        }

        this.loading = true;
        this.authService.change(this.changeForm.value)
            .subscribe(
                () => {
                    this.alertService.success('Password has been changed successfully.', true);
                    this.router.navigate(['/']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}
