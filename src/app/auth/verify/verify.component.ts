﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AuthService } from '../../@services/auth.service';
import { first } from 'rxjs/operators';
import { AlertService } from '../../@services/alert.service';

@Component({templateUrl: 'verify.component.html'})
export class VerifyComponent implements OnInit {

    returnUrl: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authService: AuthService,
        private alertService: AlertService) {}

    ngOnInit() {

        this.route.queryParams.pipe(first()).subscribe(params => {

            // verify email
            this.authService.verify({email: params['email'], token: params['token']}).subscribe(result => {
                // get return url from route parameters or default to '/'
                this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
                this.router.navigate([this.returnUrl]);
            },
            error => {
                this.alertService.error(error);
            });

        });

    }
}
