﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService } from '../../@services/alert.service';
import { FieldErrorStateMatcher } from '../../@helpers/error-state-matcher';
import { AuthService } from '../../@services/auth.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
    submitted = false;

    matcher = new FieldErrorStateMatcher();

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private authService: AuthService,
        private alertService: AlertService) { }

    firstNameFormControl = new FormControl('', [
        Validators.required,
    ]);

    lastNameFormControl = new FormControl('', [
        Validators.required,
    ]);

    emailFormControl = new FormControl('', [
        Validators.required,
        Validators.email,
    ]);

    passwordFormControl = new FormControl('', [
        Validators.required,
        Validators.minLength(6)
    ]);

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            firstName: this.firstNameFormControl,
            lastName: this.lastNameFormControl,
            email: this.emailFormControl,
            password: this.passwordFormControl
        });
    }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        this.loading = true;
        this.authService.register(this.registerForm.value)
            .pipe(first())
            .subscribe(
                () => {
                    this.alertService.success('Registration successful', true);
                    this.router.navigate(['/login']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}
