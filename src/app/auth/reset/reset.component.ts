﻿import { Component, OnInit } from '@angular/core';
import { Router, Route, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService } from '../../@services/alert.service';
import { FieldErrorStateMatcher, CrossFieldErrorStateMatcher } from '../../@helpers/error-state-matcher';
import { AuthService } from '../../@services/auth.service';
import { passwordMatchValidator } from '../../@validators/password-match.validator';

@Component({
    selector: 'app-reset',
    templateUrl: './reset.component.html',
    styleUrls: ['./reset.component.css']
})
export class ResetComponent implements OnInit {
    resetForm: FormGroup;
    loading = false;
    submitted = false;

    matcher = new FieldErrorStateMatcher();
    crossMatcher = new CrossFieldErrorStateMatcher();
    
    email: string;
    token: string;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authService: AuthService,
        private alertService: AlertService) { }

    newPasswordFormControl = new FormControl('', [
        Validators.required,
    ]);

    confirmPasswordFormControl = new FormControl('', [
        Validators.required,
    ]);

    ngOnInit() {

        this.route.queryParams.pipe(first()).subscribe(params => {
            this.email = params['email'];
            this.token = params['token'];
        });

        this.resetForm = this.formBuilder.group({
            email: this.email,
            token: this.token,
            newPassword: this.newPasswordFormControl,
            confirmPassword: this.confirmPasswordFormControl,
        }, { validator: passwordMatchValidator });
    }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.resetForm.invalid) {
            return;
        }

        this.loading = true;
        this.authService.reset(this.resetForm.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.alertService.success('Password has been changed successfully.', true);
                    this.router.navigate(['/']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}
