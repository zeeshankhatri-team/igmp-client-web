import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from '../../environments/environment';
import { SearchItem } from '../@models/search-item';
import { SearchParam } from '../@models/search-param';
import { IHttpOptions } from '../http-options';
import { PagedSorted } from '../@models/paged-sorted';
import { PagedResult } from '../@models/paged-result';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(@Inject('HttpOptions') private options: IHttpOptions, private http: HttpClient) { }

  searchUrl = environment.api + 'search';

  search(parameters: PagedSorted<SearchParam>): Observable<PagedResult<SearchItem>> {
    return this.http.post<PagedResult<SearchItem>>(this.searchUrl, parameters, this.options);
  }
}
