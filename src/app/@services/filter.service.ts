import { Injectable, Inject } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IHttpOptions } from '../http-options';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FilterService {

  constructor(@Inject('HttpOptions') private options: IHttpOptions, private http: HttpClient) { }

  filterUrl = environment.api + 'filter';

  getImporters(filter: string): Observable<string[]> {
      const url = `${this.filterUrl}/Importers?filter=${filter}`;
      return this.http.get<string[]>(url, this.options);
  }

  getConsignors(filter: string): Observable<string[]> {
    const url = `${this.filterUrl}/Consignors?filter=${filter}`;
    return this.http.get<string[]>(url, this.options);
  }

  getItems(filter: string): Observable<string[]> {
    const url = `${this.filterUrl}/Items?filter=${filter}`;
    return this.http.get<string[]>(url, this.options);
  }

  getBLNumbers(filter: string): Observable<string[]> {
    const url = `${this.filterUrl}/BLNumbers?filter=${filter}`;
    return this.http.get<string[]>(url, this.options);
  }

  getFlightNumbers(filter: string): Observable<string[]> {
    const url = `${this.filterUrl}/FlightNumbers?filter=${filter}`;
    return this.http.get<string[]>(url, this.options);
  }

  getSheds(filter: string): Observable<string[]> {
    const url = `${this.filterUrl}/Sheds?filter=${filter}`;
    return this.http.get<string[]>(url, this.options);
  }

  // getData(): Observable<string[]> {
  //   return of([
  //     'Mary',
  //     'Shelley',
  //     'Igor'
  //   ]);
  // }
}
