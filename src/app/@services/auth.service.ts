﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { User } from '../@models/user';

export interface UserToken
{
    email: string;
    firstName: string;
    lastName: string;
    token: string;
}

@Injectable()
export class AuthService {

    constructor(private http: HttpClient) { }

    authUrl = environment.api + 'auth';
    mapToken = map<UserToken, UserToken>(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('currentUser', JSON.stringify(user));
        }
        return user;
    })

    login(email: string, password: string) {
        return this.http.post<any>(`${this.authUrl}/login`, { email: email, password: password })
            .pipe(this.mapToken);
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }

    register(user: User) {
        return this.http.post(`${this.authUrl}/register`, user);
    }

    verify(value: any): any {
        return this.http.post(`${this.authUrl}/verify`, value)
            .pipe(this.mapToken);
    }

    forgot(value: any): any {
        return this.http.post(`${this.authUrl}/forgot`, value);
    }

    reset(value: any): any {
        return this.http.post(`${this.authUrl}/reset`, value)
            .pipe(this.mapToken);
    }

    change(value: any): any {
        return this.http.post(`${this.authUrl}/change`, value);
    }
}
