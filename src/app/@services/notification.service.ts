import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from '../../environments/environment';
import { IHttpOptions } from '../http-options';
import { ContinuousNotification, SingleNotification } from '../@models/notification';
import { Notification } from '../@models/notification';
import { PagedResult } from '../@models/paged-result';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(@Inject('HttpOptions') private options: IHttpOptions, private http: HttpClient) { }

  notificationUrl = environment.api + 'notifications';

  getAll(): Observable<Notification[]> {
    return this.http.get<Notification[]>(this.notificationUrl, this.options);
  }

  getAllPaged(): Observable<PagedResult<Notification>> {
    return this.http.get<PagedResult<Notification>>(this.notificationUrl, this.options);
  }

  get(id: number): Observable<Notification> {
    var url =  `${this.notificationUrl}/${id}`;
    return this.http.get<Notification>(url, this.options);
  }

  addContinuous(model: ContinuousNotification) {
    var url =  `${this.notificationUrl}/Continuous`;
    return this.http.post(url, model, this.options);
  }

  addSingle(model: SingleNotification) {
    var url =  `${this.notificationUrl}/Single`;
    return this.http.post(this.notificationUrl, model, this.options);
  }

  updateContinuous(id: number, model: ContinuousNotification) {
    var url =  `${this.notificationUrl}/Continuous/${id}`;
    return this.http.put(url, model, this.options);
  }

  updateSingle(id: number, model: SingleNotification) {
    var url =  `${this.notificationUrl}/Single/${id}`;
    return this.http.put(url, model, this.options);
  }

  delete(id: number) {
    var url =  `${this.notificationUrl}/${id}`;
    return this.http.delete(url, this.options);
  }
}