import { TestBed, async } from '@angular/core/testing';
import { BaseComponent } from './base.component';
describe('BaseComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BaseComponent
      ],
    }).compileComponents();
  }));
  it('should create the base', async(() => {
    const fixture = TestBed.createComponent(BaseComponent);
    const base = fixture.debugElement.componentInstance;
    expect(base).toBeTruthy();
  }));
  it(`should have as title 'igmp-client-web'`, async(() => {
    const fixture = TestBed.createComponent(BaseComponent);
    const base = fixture.debugElement.componentInstance;
    expect(base.title).toEqual('igmp-client-web');
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(BaseComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to igmp-client-web!');
  }));
});
